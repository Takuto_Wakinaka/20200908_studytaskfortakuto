package main

import (
	"bitbucket.org/Takuto_Wakinaka/20200908_studytaskfortakuto/server/mysql"
	"bitbucket.org/Takuto_Wakinaka/20200908_studytaskfortakuto/server/mysql/sqlboiler-mysql/models"

	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/ant0ine/go-json-rest/rest"
	"github.com/volatiletech/null"
	"github.com/volatiletech/sqlboiler/boil"
)

type MemberListModel struct {
	models.Member
}

var layout = "2006-01-02"

// Rest-api routing and Sserver Listen.
func main() {
	// Initialize DB.
	mysql.InitDB()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	router, err := rest.MakeRouter(
// 単一取得と複数取得(指定した数取りたい)
		rest.Get("/member", member_select_all),
		rest.Post("/member", member_insert_one),
		rest.Put("/member", member_update_one),
// id 指定はurlに混ぜ込める。 UrlParam, クエリパラメータ。
		rest.Delete("/memberlist", member_delete_one),
	)
	if err != nil {
		log.Fatal(err)
	}
	api.SetApp(router)

	log.Printf("Listening on port %s", port)
	if err := http.ListenAndServe(":"+port, api.MakeHandler()); err != nil {
		log.Fatal(err)
	}
}

// indexHandler responds to requests with our list of available databases.
func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	fmt.Fprintln(w, "Hello, World!")
}

//　member_select_all Get record from "Member".
func member_select_all(w rest.ResponseWriter, r *rest.Request) {

	findmember, err := models.Members().All(context.Background(), mysql.DB)

	if err == nil {
		for i:=0; i < len(findmember); i++{
			fmt.Print((*findmember[i]).ID)
			fmt.Print((*findmember[i]).Name.String)
			fmt.Println((*findmember[i]).Birthday.Time.String())
		}
	} else {
		fmt.Print(err)
	}
}

// Insert record from "Member".
func member_insert_one(w rest.ResponseWriter, r *rest.Request)  {

	// rest.Request JSON convert Insert Parameter.
	member := models.Member{}
	memberCol := models.MemberColumns

	err := r.DecodeJsonPayload(&memberCol)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if memberCol.ID == "" {
		rest.Error(w, "country code required", 400)
		return
	}
	if memberCol.Name == "" {
		rest.Error(w, "country name required", 400)
		return
	}
	if memberCol.Birthday == "" {
		rest.Error(w, "country name required", 400)
		return
	}

	t, _ := time.Parse(layout, memberCol.Birthday)

	member.ID, _ = strconv.Atoi(memberCol.ID)
	member.Name = null.StringFrom(memberCol.Name)
	member.Birthday = null.TimeFrom(t)

	err = member.Insert(context.Background(), mysql.DB, boil.Columns{})
	if err != nil {
		fmt.Println(err)
		return
	}
}

// Update record from "Member".
func member_update_one(w rest.ResponseWriter, r *rest.Request)  {

	// rest.Request JSON convert Insert Parameter.
	member := models.Member{}
	memberCol := models.MemberColumns

	err := r.DecodeJsonPayload(&memberCol)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if memberCol.ID == "" {
		rest.Error(w, "country code required", 400)
		return
	}
	if memberCol.Name == "" {
		rest.Error(w, "country name required", 400)
		return
	}
	if memberCol.Birthday == "" {
		rest.Error(w, "country name required", 400)
		return
	}

	t, _ := time.Parse(layout, memberCol.Birthday)

	member.ID, _ = strconv.Atoi(memberCol.ID)
	member.Name = null.StringFrom(memberCol.Name)
	member.Birthday = null.TimeFrom(t)

// updateってidないとどうなる???
	_, err = member.Update(context.Background(), mysql.DB, boil.Columns{})
	if err != nil {
		fmt.Println(err)
		return
	}
}

// Delete record from "Member" by ID.
func member_delete_one(w rest.ResponseWriter, r *rest.Request) {

	member := models.Member{}
	memberCol := models.MemberColumns

	err := r.DecodeJsonPayload(&memberCol)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if memberCol.ID == "" {
		rest.Error(w, "country code required", 400)
		return
	}

	member.ID, _ = strconv.Atoi(memberCol.ID)
	_, err = member.Delete(context.Background(), mysql.DB)
	if err != nil {
		fmt.Println(err)
		return
	}
}

