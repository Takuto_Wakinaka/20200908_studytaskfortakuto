module models

go 1.14

require (
	github.com/friendsofgo/errors v0.9.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/sqlboiler/v4 v4.2.0
	github.com/volatiletech/strmangle v0.0.1
)
