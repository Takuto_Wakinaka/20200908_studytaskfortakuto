package models

import (
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

// Initialize mysql connection.
func InitDB() {


	// Default "dsn" is connecting Local database.
	dsn := "Takuto:wa55529@/db_studytask?parseTime=true"

	// If app deployed. "dsn" is connecting GCP database.
	if(os.Getenv("DEPLOY_SWITCH") == "on") {
		// dsn = "Takuto_pro:wa55529@cloudsql(%s:%s)/db_studytask_pro?parseTime=true"
	}

	openedDB, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}

	openedDB.SetMaxIdleConns(10)
	openedDB.SetMaxOpenConns(10)
	openedDB.SetConnMaxLifetime(300 * time.Second)

	DB = openedDB
}
