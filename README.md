# Title
20200908_StudyTaskForTakuto.md
​
# Content
​
	参画前の学習課題
## Refarence
1. go
    + https://go-tour-jp.appspot.com/welcome/1
    + https://golang.org/doc/effective_go.html#commentary
2. REST
    + https://qiita.com/masato44gm/items/dffb8281536ad321fb08#:~:text=RESTful%20API(REST%20API)%E3%81%A8,%E3%81%AB%E5%BE%93%E3%81%A3%E3%81%A6%E7%AD%96%E5%AE%9A%E3%81%95%E3%82%8C%E3%81%9F%E3%82%82%E3%81%AE%E3%80%82
3. SQLBoiler
    + https://github.com/volatiletech/sqlboiler/blob/master/README.md
    + http://wild-data-chase.com/index.php/2020/04/05/post-1036/
​
## Set Up
1. Golandの導入

## Basic Task
1. go でハロワするアプリケーションを goland(IDE) にて作成する (以降、これをアプリケーションと呼称)
2. アプリケーションを GAE にデプロイする →GCPプロジェクトは[新しいプロジェクト] > [組織なし] の作成でいいのでしょうか? 　プロジェクトを作成したら課金アラートを設定する。
3. ローカルで作成した MySQL DB のテーブルから値を取得する機能をアプリケーションに追加する。
​
## Enhance Task
1. ORM である SQLBoiler を使用して、テーブルの値を取得する機能をアプリケーションに追加する。
2. REST 原則に則り、GET / PUT / POST の HTTP メソッドに対応した API 機能をアプリケーションに追加する。機能内容は問わない。
折りたたむ