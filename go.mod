module bitbucket.org/Takuto_Wakinaka/20200908_studytaskfortakuto

go 1.14

require (
	github.com/ant0ine/go-json-rest v3.3.2+incompatible
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/volatiletech/inflect v0.0.1 // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.7.1+incompatible
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)